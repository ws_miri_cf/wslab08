package restLayer;

import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import businessLayer.models.ModelException;
import businessLayer.models.Tweet;

import com.sun.xml.internal.messaging.saaj.util.Base64;

@Path("/tweets")
public class TweetResource {

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Collection<Tweet> findAll() {
		try {
			return Tweet.findAll();
		} catch (ModelException e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
	}

	
	@POST
	@Consumes({ MediaType.APPLICATION_JSON})
	@Produces({ MediaType.APPLICATION_JSON})
	public Tweet create(@HeaderParam(HttpHeaders.AUTHORIZATION) String authHeader, Tweet posted) {

		String[] us_pass = getUsernameAndPassord(authHeader);
		String username = us_pass[0];
		String password = us_pass[1];
		Tweet tweet = null;

		/*
		 *  Insert the new tweet here using the methods provided by
		 *  the businessLayer.models objects User and Tweet
		 *  
		 */

		return tweet;
	}

	
	
	
	private String[] getUsernameAndPassord(String secret)
			throws WebApplicationException {
		String[] result = null;
		if (secret != null && secret.toUpperCase().startsWith("BASIC ")) {
			try {
				result = Base64.base64Decode(secret.substring(6)).split(":");
			}
			catch (Exception e) { e.printStackTrace(); }
		}
		if (result == null) 
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		else 
			return result;
	}

}
