package clientSide;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class SimpleClient {


	private static String TWEET_COLLECTION_URI = "http://localhost:8080/wslab08/rest/tweets";

	public static void main(String[] args) {

		Client client = Client.create();

		/*
		 Task#2: Set authorization filter here 
		 */

		WebResource r = client.resource(TWEET_COLLECTION_URI);    

		/* 
		 * Task#2: Post New Tweet Here
		 */



		/*
		 * Task#3: Like tweet added in task#2
		 */


		ClientResponse response = r.get(ClientResponse.class);
		System.out.println("=======================================");
		System.out.println( response.getStatus() );
		JSONArray entity = response.getEntity(JSONArray.class);
		try {
			for (int i = 0; i < entity.length(); i++) 
				System.out.println(tweetToString(entity.getJSONObject(i)));
		}
		catch (JSONException e) {
			e.printStackTrace();
		}

		
		/*
		 *  Task#4: Delete tweet added in task#2
		 */

	}

	private static String tweetToString(JSONObject tweet) {
		String result = "tweet #";
		try {
			result += tweet.get("tweet_id");
			result += " | ";
			result += tweet.get("user_name");
			result += ": ";
			result += tweet.getString("text");
			result += " | ";
			result += tweet.getString("likes");
			result += " likes | ";
			result += tweet.getString("time");
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}
}
