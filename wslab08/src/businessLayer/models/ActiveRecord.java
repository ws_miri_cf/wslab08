package businessLayer.models;

import java.sql.Connection;

import businessLayer.db.H2dbStarter;

public class ActiveRecord {

	private static Connection dbConnection = null;

	protected static Connection getDbConnection() {
		if (dbConnection == null) 
			dbConnection = H2dbStarter.getDbConnection();
		return dbConnection;
	}

}
